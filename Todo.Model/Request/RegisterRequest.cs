namespace Todo.Model.Request;

public class RegisterRequest
{
    public string UserName { get; set; }
    public byte[] PasswordHash { get; set; }
    public byte[] PasswordSalt { get; set; }
}