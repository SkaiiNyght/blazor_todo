using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Todo.Model.Entity;

[Table("user")]
public class User
{
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column("UserId")]
    public int UserId { get; set; }

    [Column("UserName")]
    public string UserName { get; set; }

    [Column("PasswordHash")]
    public byte[] PasswordHash { get; set; }
    
    [Column("PasswordSalt")]
    public byte[] PasswordSalt { get; set; }
    
}