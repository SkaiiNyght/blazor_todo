using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Todo.Model.Entity;

[Table("item")]
public class TodoItem
{
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column("ItemId")]
    public int ItemId { get; set; }

    [Column("Description")]
    public string Description { get; set; }
    
    [Column("Complete")]
    public bool IsComplete { get; set; }
}