using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Security.Claims;
using System.Text.Json;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;

namespace Todo.Client;

public class CustomAuthStateProvider : AuthenticationStateProvider
{
    private readonly HttpClient _http;
    private readonly ILocalStorageService _localStorageService;

    public CustomAuthStateProvider(HttpClient http, ILocalStorageService localStorageService)
    {
        _http = http;
        _localStorageService = localStorageService;
    }

    public override async Task<AuthenticationState> GetAuthenticationStateAsync()
    {
        var token = await _localStorageService.GetItemAsStringAsync("token");
        _http.DefaultRequestHeaders.Authorization = null;
        if (string.IsNullOrEmpty(token))
        {
            token = await _http.GetFromJsonAsync<string>("api/auth/jwt");
            await _localStorageService.SetItemAsync("token", token);
            
        }

        if (!string.IsNullOrEmpty(token))
        {
            _http.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token.Replace("\"", ""));
        }
        var identity = new ClaimsIdentity(ParseClaimsFromJwt(token), "jwt");
        // Authorized User
        var user = new ClaimsPrincipal(identity);

        // Unauthorized user
        // var user = new ClaimsPrincipal();

        var state = new AuthenticationState(user);

        NotifyAuthenticationStateChanged(Task.FromResult(state));

        return state;
    }
    public static IEnumerable<Claim> ParseClaimsFromJwt(string jwt)
    {
        
        var claims = new List<Claim>();
        var payload = jwt.Split('.')[1];
        var jsonBytes = ParseBase64WithoutPadding(payload);
        var keyValuePairs = JsonSerializer.Deserialize<Dictionary<string, object>>(jsonBytes);
  
        keyValuePairs.TryGetValue(ClaimTypes.Role, out object roles);
  
        // Check if roles is an array, if it is, pull out each of the fields individually. Having the roles as an
        // Array doesn't work with <AuthorizeView Role="RoleYouWant"></AuthorizeView"
        if (roles != null)
        {
            if (roles.ToString().Trim().StartsWith("["))
            {
                var parsedRoles = JsonSerializer.Deserialize<string[]>(roles.ToString());
  
                foreach (var parsedRole in parsedRoles)
                {
                    claims.Add(new Claim(ClaimTypes.Role, parsedRole));
                }
            }
            else
            {
                claims.Add(new Claim(ClaimTypes.Role, roles.ToString()));
            }
  
            keyValuePairs.Remove(ClaimTypes.Role);
        }
  
        claims.AddRange(keyValuePairs.Select(kvp => new Claim(kvp.Key, kvp.Value.ToString())));
  
        return claims;
    }


    private static byte[] ParseBase64WithoutPadding(string base64)
    {
        switch (base64.Length % 4)
        {
            case 2: base64 += "=="; break;
            case 3: base64 += "="; break;
        }
        return Convert.FromBase64String(base64);
    } 
}