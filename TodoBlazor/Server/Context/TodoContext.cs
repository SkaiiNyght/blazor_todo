using Microsoft.EntityFrameworkCore;
using Todo.Model.Entity;

namespace Todo.Server.Context;

public class TodoContext: DbContext
{
   public TodoContext(DbContextOptions<TodoContext> options): base(options) {}
   public DbSet<TodoItem> TodoItems { get; set; }
   public DbSet<User> Users { get; set; }

   protected override void OnModelCreating(ModelBuilder modelBuilder)
   {
      modelBuilder.Entity<TodoItem>().ToTable("item");
   }
}