using Microsoft.AspNetCore.Mvc;
using Todo.Model.Entity;
using Todo.Server.services;

namespace Todo.Server.Controllers;

[ApiController]
[Route("api/v1/todoItem")]
[Produces("application/json")]
public class TodoItemController: ControllerBase
{
    private readonly ITodoItemService _service;

    public TodoItemController(ITodoItemService service)
    {
        _service = service;
    }
   
    [HttpGet, Route("")]
    public async Task<ActionResult<IEnumerable<TodoItem>>> GetAllTodoItems()
    {
        var items = await _service.GetAllTodoItems();

        return Ok(items);
    }
}