using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using NuGet.Protocol;
using Todo.Server.services;
using Todo.Shared;

namespace Todo.Server.Controllers
{
    [Route("api/auth")]
    [ApiController]
    [Authorize]
    public class AuthController : ControllerBase
    {
        private IConfiguration _config;
        private IUserService _userService;
        public AuthController(IConfiguration config, IUserService userService)
        {
            _config = config;
            _userService = userService;
        }

        [HttpGet]
        [Route("me/thecorrectway")]
        [Authorize]
        public ActionResult<string> GetMeTheGoodWay()
        {
            var username = _userService.GetMyName();
            return Ok(username);
        }
        
        
        [HttpGet]
        [Route("me")]
        [Authorize]
        public ActionResult<string> GetMe()
        {
            // This will only work when the user is passing in the token along with the Authorize attribute
            // This is not the recommended way to read data out of the claim
            var userName=  User.Identity?.Name;
            return Ok(userName);
        }
        
        [HttpGet]
        [Route("jwt")]
        [AllowAnonymous]
        public ActionResult<string> RetrieveJwt()
        {
            string token = CreateToken();
            return Ok(token.ToJson());
        }


        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult<string>> LogIn([Required, FromBody] UserDto user)
        {
            var loginResult = await _userService.Login(user);
            if (loginResult.Length > 0)
            {
                return Ok(loginResult);
            }
            else
            {
                return Unauthorized("Invalid Login");
            }
        }

        [HttpPost]
        [Route("register")]
        [AllowAnonymous]
        public async Task<ActionResult<string>> Register([Required, FromBody] UserDto user)
        {
            var registerResult = await _userService.Register(user);
            if (registerResult.Length > 0)
            {
                return Ok(registerResult);
            }
            else
            {
                return BadRequest("Unable to register user");
            }
        }

        private string CreateToken()
        {
                List<Claim> claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.Name, "SkaiiNyght"),
                    new Claim(ClaimTypes.Role, "Admin"),
                    new Claim(ClaimTypes.Role, "Edit"),
                    new Claim(ClaimTypes.Role, "Read"),
                    new Claim(ClaimTypes.Gender, "Male"),
                };

                var key = new SymmetricSecurityKey(
                    System.Text.Encoding.UTF8.GetBytes(_config["Authentication:TokenKey"]));
                var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

                var token = new JwtSecurityToken(
                    claims: claims,
                    expires: DateTime.Now.AddDays(1),
                    signingCredentials: credentials);

                var jwt = new JwtSecurityTokenHandler().WriteToken(token);
                return jwt;
            

        }
    }
}