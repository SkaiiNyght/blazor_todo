using System.Security.Cryptography;
using System.Security.Policy;
using Microsoft.EntityFrameworkCore;
using NuGet.Protocol;
using Todo.Model.Entity;
using Todo.Model.Request;
using Todo.Server.Context;

namespace Todo.Server.Repository;

public interface IUserRepository
{

    Task<User> LogIn(LoginRequest loginRequest);
    Task<User> Register(RegisterRequest registerRequest);

}

public class UserRepository: IUserRepository
{
    private readonly TodoContext _context;

    public UserRepository(TodoContext context)
    {
        _context = context;
    }
    
    public async Task<User> LogIn(LoginRequest loginRequest)
    {
        var foundUser = await _context.Users.AsNoTracking()
            .FirstOrDefaultAsync(x => x.UserName.Equals(loginRequest.UserName));
        if (foundUser != null)
        {
            using (var hmac = new HMACSHA512() {Key = foundUser.PasswordSalt})
            {
                var saltedPassword = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(loginRequest.Password));
                if (saltedPassword.ToJson().Equals(foundUser.PasswordHash.ToJson()))
                {
                    return foundUser;
                }
            }
        }

        return new User();
    }
    

    public async Task<User> Register(RegisterRequest registerRequest)
    {
        var user = new User
        {
            PasswordHash = registerRequest.PasswordHash,
            UserName = registerRequest.UserName,
            PasswordSalt = registerRequest.PasswordSalt
        };

        await _context.Users.AddAsync(user);
        await _context.SaveChangesAsync();

        return user;

    }
    
    
}