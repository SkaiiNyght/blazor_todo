using Microsoft.EntityFrameworkCore;
using Todo.Model.Entity;
using Todo.Server.Context;

namespace Todo.Server.Repository;

public interface ITodoItemRepository
{
    Task<IEnumerable<TodoItem>> GetAllTodoItems();
}

public class TodoItemRepository: ITodoItemRepository
{
    private readonly TodoContext _context;
    public TodoItemRepository(TodoContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<TodoItem>> GetAllTodoItems()
    {
        var items = await _context.TodoItems.AsNoTracking().ToListAsync();
        return items;
    }
    
}