using Todo.Model.Entity;
using Todo.Server.Repository;

namespace Todo.Server.services;

public interface ITodoItemService
{
    Task<IEnumerable<TodoItem>> GetAllTodoItems();
}

public class TodoItemService: ITodoItemService
{

    private readonly ITodoItemRepository _repository;

    public TodoItemService(ITodoItemRepository repository)
    {
        _repository = repository;
    }


    public async Task<IEnumerable<TodoItem>> GetAllTodoItems()
    {
        return await _repository.GetAllTodoItems();
    }
}