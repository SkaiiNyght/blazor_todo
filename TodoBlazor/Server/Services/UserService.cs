using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using Microsoft.IdentityModel.Tokens;
using NuGet.Protocol;
using Todo.Model.Entity;
using Todo.Model.Request;
using Todo.Server.Repository;
using Todo.Shared;

namespace Todo.Server.services;

public interface IUserService
{
    string GetMyName();
    Task<string> Register(UserDto registerRequest);
    Task<string> Login(UserDto loginRequest);

}

public class UserService: IUserService
{
    private IHttpContextAccessor _httpContextAccessor;
    private IUserRepository _userRepository;
    private IConfiguration _config;
    private ILogger<UserService> _logger;
    public UserService(IHttpContextAccessor httpContextAccessor, IUserRepository userRepository, IConfiguration config, ILogger<UserService> logger)
    {
        _httpContextAccessor = httpContextAccessor;
        _userRepository = userRepository;
        _config = config;
        _logger = logger;
    }
    
    
    /// <summary>
    /// This is the best practices way to get data out of the token
    /// </summary>
    /// <returns></returns>
    public string GetMyName()
    {
        var result = string.Empty;
        if (_httpContextAccessor.HttpContext != null)
        {
            result = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Name);
        }
        return result;
    }

    public async Task<string> Register(UserDto raw)
    {
        CreatePasswordHash(raw.Password, out byte[] passwordHash, out byte[] passwordSalt);
        var registerRequest = new RegisterRequest()
        {
            PasswordSalt = passwordSalt,
            UserName = raw.UserName,
            PasswordHash = passwordHash
        };
        var result = await _userRepository.Register(registerRequest);
        return CreateToken(result);
    }
    

    public async Task<string> Login(UserDto user)
    {
       _logger.LogInformation($"Attempting login for user {user.UserName}"); 
        var loginRequest = new LoginRequest()
        {
            UserName = user.UserName,
            Password = user.Password
        };
        var result = await _userRepository.LogIn(loginRequest);
        if (result.UserName?.Length > 0)
        {
            _logger.LogInformation($"Login attempt for user {user.UserName} was successful.");
            return CreateToken(result);
        }

        _logger.LogInformation($"Login for user {user.UserName} failed");
        return string.Empty;

    }
    

    private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
    {
        using (var hmac = new HMACSHA512())
        {
            passwordSalt = hmac.Key;
            passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
        }
    }

    private string CreateToken(User user)
    {
        List<Claim> claims = new List<Claim>()
        {
            new Claim(ClaimTypes.Name, user.UserName)
        };

        var key = new SymmetricSecurityKey(
            System.Text.Encoding.UTF8.GetBytes(_config["Authentication:TokenKey"]));
        var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

        var token = new JwtSecurityToken(
            claims: claims,
            expires: DateTime.Now.AddDays(1),
            signingCredentials: credentials);

        var jwt = new JwtSecurityTokenHandler().WriteToken(token);
        return jwt;
    }
    

}